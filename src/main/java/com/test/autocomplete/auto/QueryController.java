package com.test.autocomplete.auto;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/v1")
public class QueryController {
	
	@ModelAttribute
	public void setResponseHeader(HttpServletResponse response) {// added for CORS proxy
		response.setHeader("Access-Control-Allow-Origin", "*");
	}
	
	@GetMapping("/search")
	public List<String> getAllUsers(@RequestParam("data") String prefix) {
		return Dictionary.getInst().wordsWithPrefix(prefix);
	}

}
