package com.test.autocomplete.auto;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Dictionary {
	private static Dictionary dictionary;
	private Node root;

	private Dictionary() {
		this.root = new Node(' ');
	}

	public static Dictionary getInst() {
		if (dictionary == null) {
			dictionary = new Dictionary();
			DictionaryHelper.initDictionary(dictionary);
		}
		return dictionary;
	}

	public void addWord(String word) {
		char[] charArr = word.toLowerCase().toCharArray();
		Node temp = root;
		Node tn = null;
		int index = 0;

		do {
			tn = temp.child[Index.map.get(charArr[index])];
			if (tn != null) {// character already present
				temp = tn;
				index++;
				if (index >= word.length()) {
					temp.terminal = true;
					temp.word = word;
					return;
				}
			}
		} while (tn != null);

		// complete remaining char/word
		for (; index < charArr.length; index++) {
			temp.child[Index.map.get(charArr[index])] = new Node(charArr[index]);
			temp = temp.child[Index.map.get(charArr[index])];
		}

		temp.terminal = true;
		temp.word = word;
	}

	public List<String> wordsWithPrefix(String prefix) {
		char[] charArr = prefix.toLowerCase().toCharArray();
		Node temp = root;
		Node tn = null;
		int index = 0;
		do {
			Integer nodeIndex = Index.map.get(charArr[index]);
			if (nodeIndex == null) {
				return new ArrayList();
			}
			tn = temp.child[nodeIndex];
			// then no words with this prefix
			if (tn == null) {
				return new ArrayList();
			}

			index++;
			temp = tn;
		} while (index < charArr.length);

		// do a traversal for all paths below this.
		List<String> words = new ArrayList();
		Queue<Node> queue = new LinkedList();
		queue.add(temp);
		while (!queue.isEmpty()) {
			Node first = queue.peek();
			queue.remove();
			if (first.terminal) {
				words.add(first.word);
			}

			for (Node n : first.child) {
				if (n != null) {
					queue.add(n);
				}
			}
		}

		return words;
	}

	private static class Node {

		Node[] child;
		char label;
		boolean terminal;
		String word;

		private static int ALPHABET_SIZE = Index.map.size();

		public Node() {
			this.child = new Node[ALPHABET_SIZE];
		}

		public Node(char l) {
			this();
			this.label = l;
		}
	}

}
