package com.test.autocomplete.auto;

public class DictionaryHelper {
	
	public static void initDictionary(Dictionary dictionary) {
		dictionary.addWord("school");
		dictionary.addWord("schoolpower");
		dictionary.addWord("schoology");
		dictionary.addWord("vishal");
		dictionary.addWord("software");
		dictionary.addWord("school power");
		dictionary.addWord("software schoology");
	}

}
