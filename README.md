# README #

Technologies: Java, Spring boot, Gradle, STS.
      
How to build and run:
### Clone repo https://bitbucket.org/vishalceg/autocomplete/src/master/
### $cd autocomplete
### $gradle bootRun

Note: The server will run on port:6060.